###目录<br/>
[一.原生交互类](#000)<br/>
&nbsp;&nbsp;[1.获取当前设备类型](#getEquipmentType)<br/>
&nbsp;&nbsp;[2.注册安卓监听事件](#connectWebViewJavascriptBridge)<br/>
&nbsp;&nbsp;[3.注册Ios监听事件](#setupWebViewJavascriptBridge)<br/>
&nbsp;&nbsp;[4.添加原生方法(调用原生提供的方法)](#addNativeMethod)<br/>
&nbsp;&nbsp;[5.注册原生方法(提供方法给原生调用)](#registerNativeMethod)<br/>
[二.ajax请求](#111)<br/>
&nbsp;&nbsp;[1.获取当前设备类型](#getEquipmentType)<br/>

##
<span id='getEquipmentType'></span>
####1.获取当前设备类型:getEquipmentType(t=0, addnative=0)
```
/**
 * 获取当前设备类型
 * t<number> 只支持0 or 1,默认为0,<0:简单判断当前是ios还是android, 1:获取设备详细信息>
 * addnative<number>  0表示只单纯获取设备类型， 1表示要添加原生交互方法    
 * 
 * 返回值:{
 *      os, <ios / android>
 *      type, <mobile移动端 / tablet平板设备 / desktop（pc端）>
 *      orientation, <portrait竖向 / landscape横向 / unknown未知>
 *     }    
 */

getEquipmentType(t,addnative) || utilscw.getEquipmentType(t,addnative);
```
<span id='connectWebViewJavascriptBridge'></span>
####2.注册安卓事件监听:connectWebViewJavascriptBridge(callback)
```
connectWebViewJavascriptBridge(callback) || utilscw.connectWebViewJavascriptBridge(callback)
```

<span id='setupWebViewJavascriptBridge'></span>
####3.注册Ios事件监听:setupWebViewJavascriptBridge(callback)
```
setupWebViewJavascriptBridge(callback) || utilscw.setupWebViewJavascriptBridge(callback)
```
<span id='addNativeMethod'></span>
####4.添加原生方法(调用app提供的方法):addNativeMethod(methodname = '',data='',callback = null)
```
/**
 * 添加原生方法（调用app提供的方法）
 * @param methodname<string> 原生提供的方法名
 * @param data<string || object> 原生需要的入参
 * @param callback<function> 回调方法接受原生返回的数据
 */
addNativeMethod(methodname, data, callback) || utilscw.addNativeMethod(methodname, data, callback)
```
<span id='registerNativeMethod'></span>
####5.注册原生方法(提供方法给原生调用):registerNativeMethod(methodname = '', callback = null)
```
/**
 * 注册原生方法(提供方法给原生调用)
 * @param methodname <string> 提供给原生回调的方法名
 * @param callback<function>  回调方法接受原生返回的数据
 */
registerNativeMethod(methodname, callback) || utilscw.registerNativeMethod(methodname, callback)
```
