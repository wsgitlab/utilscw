// 原生交互的一些方法
import {
  getEquipmentType, connectWebViewJavascriptBridge, setupWebViewJavascriptBridge, addNativeMethod, registerNativeMethod,
} from '../modules/native';

import ajax from '../modules/ajax';

const a = ajax({ baseURL: 'https://apisit.saicgmac.com:8443' });

// 请求拦截方法
const requestInterceptors = function (config) {
  // console.log(config,'请求拦截')
  return config;
};
// 响应拦截方法
const responseInterceptors = function (config) {
  // console.log(config,'响应拦截');
  return config;
};

// 展示loading的方法
const loadingShow = function () {
  // console.log('展示loading')
};
// 隐藏loading方法
const loadingHidden = () => {
  // console.log('隐藏loading')
};
// 展示toast
const toastShow = () => {
  console.log('展示toast');
};
// 隐藏toast
const toastHidden = () => {
  console.log('隐藏toast');
};

// 添加请求拦截方法
a.addMethods(requestInterceptors, 'requestInterceptors')
  .addMethods(responseInterceptors, 'responseInterceptors')
  .addMethods(loadingShow, 'loadingShow')
  .addMethods(loadingHidden, 'loadingHidden')
  .addMethods(toastShow, 'toastShow')
  .addMethods(toastHidden, 'toastHidden');

// 获取请求参数
let params = window.location.href.split('?')[1].split('=');
let obj = {
  [params[0]]: params[1],
};

let mobileId = '';
let aud = '';

// 设置请求头
const setHeaders = (params) => {
  let p = params.split('.');
  let payload = JSON.parse(Base64.decode(p[1]));
  let randomNum = Math.floor(Math.random() * 99);
  let time = new Date().getTime();
  let ssg_token = params;
  let ssg_rid = `${payload.iss}.3rdparty.${time}.${randomNum}`;

  // 请求参数获取
  mobileId = payload.parameters.ecosysId;
  aud = payload.aud;

  a.setHeader({
    ssg_token,
    ssg_rid,
  });
  // console.log(a.getHeader(),'GETheader')
};

setHeaders(obj.authorization, 1);

// 获取刷新码
a.postRequest('/ecosys/token/v1.0/exchange', {
  channel: aud,
  client_id: '1327f113-d106-44b0-9424-555d0ccd8d39',
}).then((res) => {
  const { code } = res;
  if (code === 'S000') {
    const { authorization } = res.data;
    window.sessionStorage.setItem('authorization', authorization);
    setHeaders(authorization);
    console.log('请求地址获取成功');
  }
});

const getOrderStatus = (params) => a.postRequest('ecosys/v1.0/approval/order/getOrderStatus', params, 1, 1);

document.querySelector('#getinfo').addEventListener('touchstart', () => {
  // getOrderStatus({mobileId})

});

const db = (fn) => {
  let time = null;
  clearTimeout(time);
  time = setTimeout(() => {

  }, 1000);
};

export {
  getEquipmentType, // 校验当前设备类型
  connectWebViewJavascriptBridge, // 注册安卓事件
  setupWebViewJavascriptBridge, // 注册ios事件
  addNativeMethod, // 添加原生提供的方法 （调用原生方法）
  registerNativeMethod, // 注册方法，提供给原生使用
};
