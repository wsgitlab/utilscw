const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  // entry: './src/ts/index.ts',
  output: {
    filename: 'utilscw.js',
    path: path.resolve(__dirname, 'build'),
    library: 'utilscw',
    globalObject: 'this',
    libraryTarget: 'umd',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js', '.d.ts', '.json'],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
  ],

  devServer: {
    contentBase: path.resolve(__dirname, 'build'), // 默认执行打包之后的路径
    compress: true, // 启动gzip压缩，
    host: 'localhost', // 访问地址
    port: 3000, // 端口号
    // open:true, //是否自动打开浏览器
    quiet: true, // 除了初始启动信息之外的任何内容都不会被打印到控制台
    // clientLogLevel:'warning', //在控制台打印编译过程的错误信息
    hot: true, // 是否启动热更新
  },
};
