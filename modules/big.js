import Big from 'big.js';

/**
 * 解决计算精度问题
 * @param num1
 * @param num2
 * @param type
 */
const big = (num1 = 0, type = '', num2 = 0) => {
  const n1 = new Big(num1);
  const n2 = new Big(num2);
  let value = null;
  switch (type) {
    case '+':
      /* 加 */
      value = Number(n1.plus(n2));
      break;
    case '-':
      /* 减 */
      value = Number(n1.minus(n2));
      break;
    case '*':
      /* 乘 */
      value = Number(n1.times(n2));
      break;
    case '/':
      /* 除 */
      value = Number(n1.div(n2));
      break;
    case 'abs':
      /* 绝对值 */
      value = Number(n1.abs());
      break;
    case '%':
      /* 取余 */
      value = Number(n1.mod(n2));
      break;
  }
  return value;
};

export default big;
