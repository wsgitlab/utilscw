//原生交互的方法
import device from 'current-device'

// 注册安卓事件
export const connectWebViewJavascriptBridge = (callback) => {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge);
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', () => {
            callback(WebViewJavascriptBridge);
        }, false);
    }
};
// 注册ios监听事件
export const setupWebViewJavascriptBridge = (callback) => {
    if (window.WebViewJavascriptBridge) { return callback(WebViewJavascriptBridge); }
    if (window.WVJBCallbacks) { return window.WVJBCallbacks.push(callback); }
    window.WVJBCallbacks = [callback];
    let WVJBIframe = document.createElement('iframe');
    WVJBIframe.style.display = 'none';
    WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
    document.documentElement.appendChild(WVJBIframe);
    setTimeout(() => {
        document.documentElement.removeChild(WVJBIframe);
    }, 0);
};

/**
 * 获取当前设备类型
 * @param t<number>  0表示只区分当前是ios还是安卓 1表示获取当前设备的详细信息
 * @param addnative<number>  0表示只单纯获取设备类型， 1表示要添加原生交互方法
 */
export const getEquipmentType = (t= 0,addnative = 0) => {
    if(+t !==0 && +t !== 1 ){
        throw '入参只支持0或1'
    }
    const os = device.os; // ios android
    const type = device.type; //mobile移动端  tablet平板设备  desktop（pc端）
    const orientation = device.orientation; //portrait竖向  landscape横向 unknown未知
    //注册安卓监听(初始化jsBridge)
    if(+addnative === 1 && os === 'android'){
        connectWebViewJavascriptBridge((bridge) => {
            bridge.init((message, responseCallback) => {});
        });
    }
    return +t === 0 ? {os} : {os, type, orientation}
};

/**
 * 添加原生方法（调用app提供的方法）
 * @param methodname<string> 原生提供的方法名
 * @param data<string || object> 原生需要的入参
 * @param callback<function> 回调方法接受原生返回的数据
 */
export const addNativeMethod = (methodname = '',data='',callback = null)=>{
    const {os} = getEquipmentType();
    const params = typeof data === 'object' ? {...data} : {data};
    if(os === 'ios'){
        //ios设备
        setupWebViewJavascriptBridge((bridge) => {
            bridge.callHandler(methodname, params, (res = null) => {
                if(res && callback){
                    //执行回调方法，将参数返回
                    callback(res);
                }
            })
        })
    }else if(os === 'android'){
        //安卓设备
        connectWebViewJavascriptBridge((bridge) => {
            bridge.callHandler(methodname, params, (res = null) => {
                if(res && callback){
                    //执行回调方法，将参数返回
                    callback(res);
                }
            })
        })
    }
}

/**
 * 注册原生方法(提供方法给原生调用)
 * @param methodname <string> 提供给原生回调的方法名
 * @param callback<function>  回调方法接受原生返回的数据
 */
export const registerNativeMethod = (methodname = '', callback = null) => {
    const {os} = getEquipmentType();
    if(os === 'ios'){
        //ios
        setupWebViewJavascriptBridge((bridge) => {
            bridge.registerHandler(
                methodname, (data = null) => {
                    if (data) callback(data);
                },
            );
        });
    }else if(os === 'android'){
        //安卓
        connectWebViewJavascriptBridge((bridge) => {
            bridge.registerHandler(
                methodname, (data = null) => {
                    if (data) callback(data);
                },
            );
        });
    }
}


