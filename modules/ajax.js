import axios from 'axios'

class Ajax {
    constructor(params ={} ) {
        //初始化的默认配置
        let base = {
            baseURL: '', // 默认请求地址
            withCredentials: false,
            timeout: 30000, // 超时时限
            retry: 0, // 请求失败，重新请求次数
            retryDelay: 1000, // 重新请求间隔时间
        }
        base = Object.assign(base,params)
        //1. 初始化axios
        const request = axios.create(base);
        this.header = {'Content-Type':'application/json;charset=UTF-8'}; //请求头信息
        this.loadingShow = null; // 展示loading方法
        this.loadingHidden = null; // 隐藏loading的方法
        this.toastShow = null; // 展示Toast
        this.toastHidden = null; // 隐藏Toast

        //2. 请求拦截
        this.requestInterceptors = null; // 请求拦截，修改请求参数
        request.interceptors.request.use((config)=> new Promise(resolve => {
            // 显示加载状态
            if(this.loadingShow) {this.loadingShow();}
            if(this.requestInterceptors !== null){
                // 开始请求参数修改
               const fnType = this.requestInterceptors instanceof Function ? 'function' : 'promise';
               switch (fnType){
                   case 'function':
                       // 返回修改过的请求数据
                       resolve(this.requestInterceptors(config));
                       break;
                   case 'promise':
                       this.requestInterceptors(config).then((resConfig)=>{
                           // 返回修改过的请求数据
                           resolve(resConfig);
                       })
                       break;
               }
            }else{
                resolve(config); //返回请求头信息
            }
        }),(err)=> Promise.reject(err))

        //3. 响应拦截
        this.responseInterceptors = null;
        request.interceptors.response.use((res)=> new Promise(resolve => {
            // 请求成功
            if(this.loadingHidden !== null){this.loadingHidden()}
            // 统一错误处理
            if(this.responseInterceptors !== null) {this.responseInterceptors(res)}
            resolve(res);
        }),(err)=>{
            // 请求失败
            if(this.loadingHidden !== null){this.loadingHidden()}

            const { response, config } = err; // 请求结果/请求配置
            let { status, data } = response;
            // 统一错误处理
            if(this.responseInterceptors !== null) {this.responseInterceptors(response)}

            // 尝试三次重试
            if (!config || !config.retry) return Promise.reject(err);
            config.__retryCount = config.__retryCount || 0;
            if (config.__retryCount >= config.retry) {
                return Promise.reject(err);
            }
            config.__retryCount += 1;
            const backoff = new Promise(((resolve) => {
                setTimeout(() => {
                    resolve();
                }, config.retryDelay || 1);
            }));
            return backoff.then(() => request(config)); // 重新发送请求
        })

        // 默认当前可以发送请求
        this.checkRequest = true;
        this.requestTimeout = null; //请求计时器
        // 设置请求状态(防抖处理)
        const setCheckRequest = (debounce = 0) => {
            if(!this.checkRequest) clearTimeout(this.requestTimeout); //清除计时器
            switch (+debounce){
                case 0:
                    this.checkRequest = false;
                    break;
                case 1:
                    this.requestTimeout = setTimeout(()=>{
                        this.checkRequest = true;
                    },1500)
                    break;
            }
        }

        const getHead = (requestType = 0) => {
            switch (+requestType){
                case 0:
                    this.header['Content-Type'] = 'application/json;charset=UTF-8';
                    break;
                case 1:
                    this.header['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
                    break;
                case 2:
                    this.header['Content-Type'] = 'multipart/form-data';
                    break;
            }
            console.log(this.header,'header')
            return this.header;
        }

        /**
         * post请求
         * @param url 请求地址
         * @param params 入参对象
         * @param debounce 是否加防抖操作 0不加 1加
         * @param requestType 0是json格式 1是文件格式 2是formdata
         * @returns {Promise<unknown>}
         */
        this.postRequest = (url, params = {}, debounce = 0, requestType = 0) => new Promise((resolve, reject) => {
            if(this.checkRequest){
                if(+debounce === 1) setCheckRequest(0); //将请求状态设置为请求中
                console.log('请求了')
                request.post(url, params, {
                    headers: getHead(requestType), // 默认是json格式
                }).then((res)=>{
                    if(+debounce === 1) setCheckRequest(1);
                    resolve(res.data);
                }).catch((err)=>{
                    if(+debounce === 1) setCheckRequest(1);
                    reject(err.response);
                })
            }
        })

        /**
         * get请求
         * @param url 请求地址
         * @param params 参数对象
         * @param debounce 是都需要防抖 0不需要 1需要
         * @param requestType 请求类型 0是json格式 1是文件格式 2是formdata
         * @returns {Promise<unknown>}
         */
        this.getRequest = (url, params={}, debounce = 0,  requestType = 0)=>new Promise((resolve, reject) => {
            if(this.checkRequest){
                if(+debounce === 1) setCheckRequest(0);
                request.get(url,{params, headers:getHead(requestType)}).then((res)=>{
                    if (+debounce === 1) setCheckRequest(1);
                    resolve(res.data);
                }).catch((err)=>{
                    if(+debounce === 1) setCheckRequest(1);
                    reject(err.response);
                })
            }
        })
    }

    // 添加方法
    addMethods(fn = null, type = ''){
        // 支持添加的方法类型
        const types = ['requestInterceptors', 'responseInterceptors','loadingShow','loadingHidden','toastShow','toastHidden'];
        if(fn instanceof Function || fn instanceof Promise){
            if(types.includes(type)){
                // 添加请求拦截或者响应拦截方法
               this[`${type}`] = fn;
            }else {
                console.warn('addMethods当前只支持添加以下类型方法：');
                console.table([
                    {type:'requestInterceptors',info:'添加请求拦截方法'},
                    {type:'responseInterceptors',info:'添加响应拦截方法'},
                    {type:'loadingShow',info:'添加展示loading方法'},
                    {type:'loadingHidden',info:'添加隐藏loading方法'},
                    {type:'toastShow',info:'添加展示toast方法'},
                    {type:'toastHidden',info:'添加隐藏toast方法'},
                ]);
            }
        }else {
            throw 'addMethods只支持添加function或者promise方法'
        }
        return this;
    }

    //查询请求头信息
    getHeader(){
        return this.header;
    }
    //设置请求头
    setHeader(params = {}){
        if(params instanceof Object){
            this.header = Object.assign(this.header, params);
        }else {
            throw '传入的参数必须为Object格式的请求头信息'
        }
    }
    //清空请求头
    clearHeader(){
        this.header = {'Content-Type':'application/json;charset=UTF-8'};
    }
}

export default (params) => {
    return new Ajax(params)
}
